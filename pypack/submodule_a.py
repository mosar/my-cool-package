# -*- coding: utf-8 -*-
"""Submodule A in pypack

Author
------
Jenni Rinker
rink@dtu.dk
"""

# place any imports at the top of the file


def double(x):
    """Double a number

    Arguments
    ---------
    x : int, float
        The number to be doubled.

    Returns
    -------
    y : int, float
        Double the input.
    """
    return 2 * x

def square(x):
    """Double a number

    Arguments
    ---------
    x : int, float
        The number to be doubled.

    Returns
    -------
    y : int, float
        Double the input.
    """
    return x * x

def helloworld():
    print('hello world')
    return